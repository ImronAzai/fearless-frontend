import React, { useEffect, useState } from 'react';

function ConferenceForm(props){
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [start, setStart] = useState('');
    const [end, setEnd] = useState('');
    const [description, setDescription] = useState('');
    const [maxPresentations, setMax] = useState('');
    const [maxAttendees, setAttendees] = useState('');
    const [location, setLocation] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.name = name;
        data.starts = start;
        data.ends = end;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location
        console.log(data)

        const conferenceUrl = "http://localhost:8000/api/conferences/"
        const fetchConfig = {
            method:"post",
            body: JSON.stringify(data),
            headers: {
                'Content-type': 'application/json'
            },
        }
        const conferenceResponse = await fetch(conferenceUrl, fetchConfig)
        if (conferenceResponse.ok){
            const newConference = await conferenceResponse.json()
            console.log(newConference)

            setName('');
            setStart('');
            setAttendees('');
            setDescription('');
            setEnd('');
            setLocation('');
            setMax('');
        }


    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleStartChange = (event) => {
        const value = event.target.value;
        setStart(value);
    }

    const handleEndChange = (event) => {
        const value = event.target.value;
        setEnd(value);
    }

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handleMaxChange = (event) => {
        const value = event.target.value;
        setMax(value);
    }

    const handleAttendeeChange = (event) => {
        const value = event.target.value;
        setAttendees(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }






    const fetchData = async() => {
        const location_url = "http://localhost:8000/api/locations/"
        const location_response = await  fetch(location_url)

            if (location_response.ok){
                const data = await location_response.json()
                setLocations(data.locations)
            }
    }
    useEffect(() => {
        fetchData();
    }, []);



    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" id="name" name="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={start} onChange={handleStartChange} placeholder="Starts" required type="date" id="starts" name="starts" className="form-control"/>
                <label htmlFor="Starts">Start Date</label>
              </div>
              <div className="form-floating mb-3">
                <input value={end} onChange={handleEndChange} placeholder="Ends" required type="date" id="ends" name="ends" className="form-control"/>
                <label htmlFor="Ends">End Date</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description" className="form-label">Description</label>
                <textarea value={description} onChange={handleDescriptionChange} className="form-control" id="description" name="description" rows="4"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input value={maxPresentations} onChange={handleMaxChange} placeholder="Maximum presentations" required type="number" id="max_presentations" name="max_presentations" className="form-control"/>
                <label htmlFor="max-presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input value={maxAttendees} onChange={handleAttendeeChange} placeholder="Maximum attendees" required type="number" id="max_attendees" name="max_attendees" className="form-control"/>
                <label htmlFor="max-attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select  onChange={handleLocationChange} required id="location" name='location' className="form-select">
                  <option  value="">Choose a Location</option>
                  {locations.map(location =>{
                    return (
                        <option key={location.id} value={location.id}>
                            {location.name}
                        </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )



}

export default ConferenceForm;
